# README

## Default Let's Snap design For Snapmirror

In this repository, you'll find the default designs for the strips and background of the Snapmirror.

For questions: info@letssnap.be

The Let's Snap team.
