/**
 * Script to crop a file to a single strip
 *
 * @author Tim De Paepe <tim.depaepe@letssnap.be>
 */

var doc = app.activeDocument;

var bounds = [18, 41, 594, 1788];
doc.crop(bounds);

saveSingle();

/**
 * Save Strip
 */


/**
 * Save For Web
 */

function saveForWeb(saveFile, format, quality)
{
 var sfwOptions = new ExportOptionsSaveForWeb();
    sfwOptions.format = format;
    sfwOptions.includeProfile = false;
    sfwOptions.interlaced = 0;
    sfwOptions.optimized = true;
    sfwOptions.quality = quality; //0-100
 app.activeDocument.exportDocument(saveFile, ExportType.SAVEFORWEB, sfwOptions);
}

/**
 * Save single
 */

function saveSingle()
{
  var doc = app.activeDocument;
  var name = doc.name.split(".");
  var name = name[0];
  var file = new File(doc.path + '/' + name + '_single.png');

  saveForWeb(file, SaveDocumentType.PNG, 100);
}
