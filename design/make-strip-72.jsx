/**
 * Script to crop a file to a single strip
 *
 * @author Tim De Paepe <tim.depaepe@letssnap.be>
 */

var doc = app.activeDocument;

hideDemoLayer();
saveStrip();

/**
 * Hide DEMO pictures
 */

 function hideDemoLayer()
 {
    // get the demo group
    var demoGroup = doc.layerSets.getByName('DEMO');
    demoGroup.visible = false;
 }


/**
 * Save For Web
 */

function saveForWeb(saveFile, format, quality)
{
 var sfwOptions = new ExportOptionsSaveForWeb();
    sfwOptions.format = format;
    sfwOptions.includeProfile = false;
    sfwOptions.interlaced = 0;
    sfwOptions.optimized = true;
    sfwOptions.quality = quality; //0-100
 app.activeDocument.exportDocument(saveFile, ExportType.SAVEFORWEB, sfwOptions);
}

/**
 * Save single
 */

function saveStrip()
{
  var name = "strip_72";
  var file = new File(doc.path + '/export/' + name + '.png');

  saveForWeb(file, SaveDocumentType.PNG, 100);
}
